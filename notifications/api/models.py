from django.db import models

from phonenumber_field.modelfields import PhoneNumberField
from timezone_field import TimeZoneField


class Mailing(models.Model):
    """Mailing entity"""

    id = models.BigAutoField(primary_key=True)
    created = models.DateTimeField(auto_now_add=True)
    text = models.TextField(max_length=256)
    expire = models.DateTimeField()

    def __str__(self):
        return f"({self.id}) - {self.text}"


class Client(models.Model):
    """Client entity"""

    id = models.BigAutoField(primary_key=True)
    operator = models.TextField(max_length=8)
    phone = PhoneNumberField()
    tag = models.TextField(max_length=16)
    time_zone = TimeZoneField(default="Europe/Moscow")

    def __str__(self):
        return f"({self.id}) {self.phone}"


class Message(models.Model):
    """Message entity"""

    STATUSES = [
        ("200", "Success"),
        ("201", "Created"),
        ("400", "Bad Request"),
    ]
    id = models.BigAutoField(primary_key=True)
    created = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=3, choices=STATUSES, default="201")
    mailing_id = models.ForeignKey(
        Mailing, on_delete=models.CASCADE, related_name="mailing"
    )
    client_id = models.ForeignKey(
        Client, on_delete=models.CASCADE, related_name="client"
    )

    def __str__(self):
        return f"{self.id} - {self.status}"
