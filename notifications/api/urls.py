from django.urls import path
from rest_framework.authtoken import views as auth

from . import views

urlpatterns = [
    path("", views.api_list),
    path("client/create", views.add_client),
    path("client/update", views.upd_client),
    path("client/delete/<int:client_id>", views.rm_client),
    path("mail/create", views.add_mail),
    path("mail/update/<int:mailing_id>", views.upd_mail),
    path("mail/delete/<int:mailing_id>", views.rm_mail),
    path("mail/stats", views.mail_stats),
    path("mail/stats/<int:mailing_id>", views.mail_stats_by_id),
    path("token", auth.obtain_auth_token),
]
