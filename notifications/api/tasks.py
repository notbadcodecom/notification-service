import requests
from datetime import datetime as dt

from celery import shared_task

from .models import Mailing, Client, Message

TOKEN = os.getenv('TOKEN')
MESSAGE_HEADER = {"Authorization": "Bearer " + TOKEN}
BASE_URL = "https://probe.fbrq.cloud/v1/send/"


@shared_task(name="send_message")
def send_message(client_id, mailing_id):
    mailing = Mailing.objects.get(id=mailing_id)
    client = Client.objects.get(id=client_id)
    message = Message.objects.get_or_create(
        mailing_id=mailing, client_id=client
    )[0]

    context = {
        "id": message.id,
        "phone": str(client.phone),
        "text": mailing.text,
    }

    while message.status != 200:
        if mailing.expire.timestamp() < dt.now().timestamp():
            break
        status_code = requests.post(
            f"{BASE_URL}{message.id}", json=context, headers=MESSAGE_HEADER
        ).status_code
        message.status = status_code
        message.save()
