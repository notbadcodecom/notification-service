from django.contrib import admin

from .models import Mailing, Client, Message


class MailingAdmin(admin.ModelAdmin):
    fields = ["text", "expire"]


class ClientAdmin(admin.ModelAdmin):
    fields = ["operator", "phone", "tag", "time_zone"]


class MessageAdmin(admin.ModelAdmin):
    fields = ["status", "mailing_id", "client_id"]


admin.site.register(Mailing, MailingAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(Message, MessageAdmin)
