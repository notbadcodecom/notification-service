import datetime as dt

from rest_framework.decorators import api_view, permission_classes
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from rest_framework import serializers
from rest_framework import status

from .models import Client, Mailing, Message
from .serializers import ClientSerializer, MailingSerializer
from .tasks import send_message

MIN_MAILER_EXPIRE = 24


@api_view(["POST"])
def add_client(request):
    if Client.objects.filter(phone=request.data.get("phone")).exists():
        raise serializers.ValidationError({"detail": "Phone already exists."})

    client = ClientSerializer(data=request.data)
    if client.is_valid():
        client.save()
        return Response(client.data, status=status.HTTP_201_CREATED)
    return Response(client.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(["PUT"])
def upd_client(request):
    if Client.objects.filter(phone=request.data.get("phone")).exists():
        raise serializers.ValidationError({"detail": "Phone already exists."})

    client = get_object_or_404(Client, id=request.data.get("id"))
    updated_client = ClientSerializer(instance=client, data=request.data)

    if updated_client.is_valid():
        updated_client.save()
        return Response(updated_client.data)
    return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(["DELETE"])
def rm_client(request, client_id):
    client = get_object_or_404(Client, id=client_id)
    client.delete()
    return Response(status=status.HTTP_202_ACCEPTED)


@api_view(["POST"])
def add_mail(request):
    mailing_data = {
        "text": request.data.get("text"),
        "created": request.data.get("created"),
    }
    if request.data.get("expire") is None:
        mailing_data["expire"] = dt.datetime.now().replace(microsecond=0) \
                                 + dt.timedelta(hours=MIN_MAILER_EXPIRE)
    else:
        mailing_data["expire"] = request.data.get("expire")
    mailing = MailingSerializer(data=mailing_data)
    if mailing.is_valid():
        mailing.save()
        filter_of_clients = request.data.get("filter")
        if filter_of_clients.get("type") == "tag":
            clients = Client.objects.filter(tag=filter_of_clients.get("value"))
        elif filter_of_clients.get("type") == "operator":
            clients = Client.objects.filter(
                operator=filter_of_clients.get("value")
            )
        else:
            clients = Client.objects.all()
        for client in clients:
            send_message(client.id, mailing.data.get("id"))
            send_message.apply_async(
                kwargs={
                    "client_id": client.id,
                    "mailing_id": mailing.data.get("id")
                },
                eta=mailing.data.get("created"),
            )
        return Response(mailing.data, status=status.HTTP_201_CREATED)
    return Response(mailing.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(["PUT"])
def upd_mail(request):
    mailing = get_object_or_404(Client, id=request.data.get("id"))
    mailing_data = request.data
    if mailing_data.get("expire") is None:
        mailing_data["expire"] = dt.datetime.now().replace(microsecond=0) \
                                 + dt.timedelta(hours=MIN_MAILER_EXPIRE)
    updated_mailing = MailingSerializer(instance=mailing, data=mailing_data)

    if updated_mailing.is_valid():
        updated_mailing.save()
        return Response(updated_mailing.data)
    return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(["DELETE"])
def rm_mail(request, mailing_id):
    mailing = get_object_or_404(Mailing, id=mailing_id)
    mailing.delete()
    return Response(status=status.HTTP_202_ACCEPTED)


@api_view(["GET"])
def mail_stats(request):
    context = {
        "mailings": Mailing.objects.count(),
        "messages": {}
    }
    for code, name in Message.STATUSES:
        messages = Message.objects.filter(status=code)
        context["messages"][f"{code} {name}"] = messages.count()
    return Response(context, status=status.HTTP_200_OK)


@api_view(["GET"])
def mail_stats_by_id(request, mailing_id):
    context = {}
    for code, name in Message.STATUSES:
        messages = Message.objects.filter(mailing_id=mailing_id, status=code)
        context[f"{code} {name}"] = messages.count()
    return Response(context, status=status.HTTP_200_OK)


@api_view(["GET"])
@permission_classes([IsAuthenticatedOrReadOnly])
def api_list(request):
    context = {
        "Create client": "api/client/create",
        "Update client": "api/client/update",
        "Delete client": "api/client/delete/{clientID}",
        "Create mailing": "api/mail/create",
        "Update mailing": "api/mail/update/{mailingID}",
        "Delete mailing": "api/mail/delete/{mailingID}",
        "Common mailings stats": "api/mail/stats",
        "Detailed mailing stats": "api/mail/stats/{mailingID}",
        "Get token": "api/token"
    }
    return Response(context, status=status.HTTP_200_OK)
