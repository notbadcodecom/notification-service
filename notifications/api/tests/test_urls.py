from django.test import TestCase, Client


class APITests(TestCase):
    def setUp(self):
        self.guest_client = Client()

    def test_api_list(self):
        """ Checking that the app is working """
        guest_client = Client()
        response = guest_client.get('/api/')
        self.assertEqual(response.status_code, 200)
