from rest_framework import serializers

from timezone_field.rest_framework import TimeZoneSerializerField

from .models import Client, Mailing


class ClientSerializer(serializers.ModelSerializer):
    time_zone = TimeZoneSerializerField()

    class Meta:
        model = Client
        fields = ["id", "operator", "phone", "tag", "time_zone"]


class MailingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mailing
        fields = ["id", "created", "text", "expire"]
