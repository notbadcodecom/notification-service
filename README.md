# Notification Service

## Запуск сервиса

- Склонировать сервис (.env фаил вопреки здравому смыслу залит в репозиторий)
    ```
    git clone https://gitlab.com/notbadcodecom/notification-service.git
    ```
- Запустить docker-compose
    ```
    docker-compose up
    ```
- Выполнить миграции и создать супер пользователя
    ```
    docker exec -it notifications_api_1 bash
    python manage.py migrate
    python manage.py createsuperuser
    ```
- Запросить токен /api/token



## Перечень api
- GET ​/api/ получение списка комманд

- POST ​/api/token получение токена
    ```
    Request body:
    {
        "username": "user",
        "password": "password"
    }

    ```

- POST ​/client/create создание клиента для рассылок
    ```
    Request body:
    {
        "operator": "self_operator",
        "phone": "+79990000000",
        "tag": "some tag",
        "time_zone": "Europe/Moscow"
    }

    ```

- PUT ​/client/update обновление клиента
    ```
    Request body:
    {
        "operator": "other_operator",
        "phone": "+76660000000",
        "tag": "some tag",
        "time_zone": "Europe/Moscow"
    }
    ```

- DELETE ​/client/delete/{clientId} удаление клиента
    ```
    clientId   ID клиента
    ```

- POST ​/mail/create создание рассылки
    ```
    Request body:
    {
        "created": "start_time or empty if now",
        "text": "text",
        "expire": "end_time or empty if 24 hours" 
        "filter": 
            {
                "type": "tag or operator",
                "value": "tag or operator",
            }
    }

    ```

- PUT ​/mail/update обновление рассылки
    ```
    Request body:
    {
        "created": "start_time or empty if now",
        "text": "text",
        "expire": "end_time or empty if 24 hours" 
        "filter": 
            {
                "type": "tag or operator",
                "value": "tag or operator",
            }
    }
    ```

- DELETE ​/mail/delete/{mailId} удаление клиента
    ```
    mailId   ID рассылки
    ```

- GET ​/mail/stats получение общей статистики по рассылкам

- GET ​/mail/stats/{mailId} получение статистики по рассылке
    ```
    mailId   ID рассылки
    ```
